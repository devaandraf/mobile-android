package com.wmi.substock1.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wmi.substock1.R;
import com.wmi.substock1.model.Kurir;
import com.wmi.substock1.model.ListKurir;

import java.util.ArrayList;

public class MyAdapter_Kurir extends RecyclerView.Adapter <MyAdapter_Kurir.MyHolder> {

    OnClickListener onClick;
    ArrayList<ListKurir> kurirs;

    public void setOnclick(MyAdapter_Kurir.OnClickListener onclick) {
        this.onClick = onclick;
    }

    public void setKurirs(ArrayList<ListKurir> kurirs)
    {
        this.kurirs = kurirs;
        notifyDataSetChanged();
    }

    public void clearKurirs() {
        kurirs.clear();
        notifyDataSetChanged();

    }

    public MyAdapter_Kurir() {kurirs = new ArrayList<>();}

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.double_row, parent, false);

        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {

        ListKurir kurir = kurirs.get(position);

        holder.status.setText(kurir.getStatus_pengiriman());
        holder.no_resi.setText(kurir.getResi());
        holder.tujuan.setText(kurir.getTujuan());
        if (kurir.getTujuan().equals("Gudang"))
        {
            holder.tujuan.setText(kurir.getNama_gudang());
        }
        else
            {
                holder.tujuan.setText(kurir.getNama_customer());
            }
        holder.tanggal_keluar.setText(kurirs.get(position).getTanggal_keluar());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.onClick(kurir);
            }
        });
    }

    @Override
    public int getItemCount() {
        return kurirs.size();
    }

    public interface OnClickListener
    {
        public void onClick(ListKurir listKurir);
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        TextView tanggal_keluar, tujuan, no_resi, status;

        MyHolder(@NonNull View itemView) {
            super(itemView);

            this.tujuan = itemView.findViewById(R.id.tv_tujuan);
            this.no_resi = itemView.findViewById(R.id.tv_resi);
            this.status = itemView.findViewById(R.id.tv_status);
            this.tanggal_keluar = itemView.findViewById(R.id.tv_tanggal_keluar);
        }
    }
}

