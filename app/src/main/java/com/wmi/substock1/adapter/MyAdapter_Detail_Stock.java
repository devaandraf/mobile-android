package com.wmi.substock1.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wmi.substock1.R;
import com.wmi.substock1.model.ItemUnik;

import java.util.ArrayList;

public class MyAdapter_Detail_Stock extends RecyclerView.Adapter<MyAdapter_Detail_Stock.MyHolder_Detail> {

    ArrayList<ItemUnik> uniks;

    public void setUniks(ArrayList<ItemUnik> uniks) {
        this.uniks = uniks;
        notifyDataSetChanged();
    }

    public MyAdapter_Detail_Stock() {
        uniks = new ArrayList<>();
    }

    @NonNull
    @Override
    public MyHolder_Detail onCreateViewHolder (@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);

        return new MyHolder_Detail(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder_Detail holder2, int position)
    {
        holder2.tanggal_kadaluarsa.setText(uniks.get(position).getKadaluarsa());
        holder2.kode_unik.setText(uniks.get(position).getKode_unik());
        holder2.no_lot.setText(uniks.get(position).getNomor_lot());
        holder2.jumlah_isi.setText(uniks.get(position).getJumlah_isi());
        holder2.satuan_isi.setText(uniks.get(position).getSatuan_isi());
    }

    @Override
    public int getItemCount()
    {
        return uniks.size();
    }

    public class MyHolder_Detail extends RecyclerView.ViewHolder {

        TextView kode_unik, jumlah_isi, satuan_isi, tanggal_kadaluarsa, no_lot;

        public MyHolder_Detail(@NonNull View itemView) {
            super(itemView);

            this.kode_unik = itemView.findViewById(R.id.tv_kode_unik);
            this.jumlah_isi = itemView.findViewById(R.id.tv_jumlah_isi);
            this.tanggal_kadaluarsa = itemView.findViewById(R.id.tv_tanggal_kadaluarsa);
            this.no_lot = itemView.findViewById(R.id.tv_no_lot_sn);
            this.satuan_isi = itemView.findViewById(R.id.tv_satuan_isi);
        }
    }
}


