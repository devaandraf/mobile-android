package com.wmi.substock1.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wmi.substock1.R;
import com.wmi.substock1.model.DetailBarangKurir;
import com.wmi.substock1.model.DetailKurir;
import com.wmi.substock1.model.ItemUnik;
import com.wmi.substock1.model.ListKurir;

import java.util.ArrayList;

public class MyAdapter_Detail_Kurir extends RecyclerView.Adapter<MyAdapter_Detail_Kurir.MyHolder_Detail_Kurir> {

    ArrayList<DetailBarangKurir> listKurirs;

    public void setListKurirs(ArrayList<DetailBarangKurir> listKurirs) {
        this.listKurirs = listKurirs;
        notifyDataSetChanged();
    }

    public MyAdapter_Detail_Kurir() {
        listKurirs = new ArrayList<>();
    }

    @NonNull
    @Override
    public MyHolder_Detail_Kurir onCreateViewHolder (@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.triple_row, parent, false);

        return new MyHolder_Detail_Kurir(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder_Detail_Kurir holder, int position) {

        DetailBarangKurir detailBarangKurir = listKurirs.get(position);

        holder.namabarang.setText(detailBarangKurir.getNama_produk());
        holder.total.setText(detailBarangKurir.getJumlah_isi());
        holder.brand.setText(detailBarangKurir.getBrand());
        holder.kodebarang.setText(detailBarangKurir.getKode_produk());
        holder.satuanisi.setText(detailBarangKurir.getSatuan_isi());
    }

    @Override
    public int getItemCount()
    {
        return listKurirs.size();
    }

    public class MyHolder_Detail_Kurir extends RecyclerView.ViewHolder {

        TextView namabarang, total, brand, kodebarang, satuanisi;

        public MyHolder_Detail_Kurir(@NonNull View itemView) {
            super(itemView);

            this.namabarang = itemView.findViewById(R.id.tv_nama_barang);
            this.total = itemView.findViewById(R.id.tv_total);
            this.brand = itemView.findViewById(R.id.tv_brand);
            this.kodebarang = itemView.findViewById(R.id.tv_kode_barang);
            this.satuanisi = itemView.findViewById(R.id.tv_satuan_isi);
        }
    }
}
