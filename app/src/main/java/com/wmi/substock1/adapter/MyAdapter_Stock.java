package com.wmi.substock1.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.wmi.substock1.R;
import com.wmi.substock1.model.ItemStock;

import java.util.ArrayList;

public class MyAdapter_Stock extends RecyclerView.Adapter <MyAdapter_Stock.MyHolder> {

    OnClickListener onclick;
    ArrayList<ItemStock> stocks;

    public void setOnclick(OnClickListener onclick) {
        this.onclick = onclick;
    }

    public void setStocks(ArrayList<ItemStock> stocks) {
        this.stocks = stocks;
        notifyDataSetChanged();
    }

    public void clearStocks() {
        stocks.clear();
        notifyDataSetChanged();

    }

    public MyAdapter_Stock()
    {
        stocks = new ArrayList<>();
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder (@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row, parent, false);

        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyHolder holder, final int position) {

        holder.nama_barang.setText(stocks.get(position).getNama_produk());
        holder.brand.setText(stocks.get(position).getBrand());
        holder.kode_barang.setText(stocks.get(position).getKode_produk());
        holder.kategori.setText(stocks.get(position).getKategori());
        holder.total.setText(stocks.get(position).getTotal());
        holder.satuan_isi.setText(stocks.get(position).getSatuan_isi());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onclick.onclick(stocks.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return stocks.size();
    }

    public interface OnClickListener
    {
        public void onclick(ItemStock itemStock);
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        TextView nama_barang, brand, kategori, kode_barang, total, satuan_isi;

        MyHolder(@NonNull View itemView) {
            super(itemView);

            this.nama_barang = itemView.findViewById(R.id.tv_nama_barang);
            this.brand = itemView.findViewById(R.id.tv_brand);
            this.kategori = itemView.findViewById(R.id.tv_kategori);
            this.kode_barang = itemView.findViewById(R.id.tv_kode_barang);
            this.total = itemView.findViewById(R.id.tv_total);
            this.satuan_isi = itemView.findViewById(R.id.tv_satuan_isi);
        }
    }
}
