package com.wmi.substock1.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListKurir {

    private int id;
    private int id_status;
    private int id_pengirim;
    private int id_pegawai;

    public int getId_pegawai() {
        return id_pegawai;
    }

    public void setId_pegawai(int id_pegawai) {
        this.id_pegawai = id_pegawai;
    }

    private String resi;
    private String tujuan;
    private String status_pengiriman;
    private String tanggal_keluar;
    private String nama_gudang;
    private String nama_customer;

    public String getNama_gudang() {
        return nama_gudang;
    }

    public void setNama_gudang(String nama_gudang) {
        this.nama_gudang = nama_gudang;
    }

    public String getNama_customer() {
        return nama_customer;
    }

    public void setNama_customer(String nama_customer) {
        this.nama_customer = nama_customer;
    }

    public String getTanggal_keluar() {
        return tanggal_keluar;
    }

    public void setTanggal_keluar(String tanggal_keluar) {
        this.tanggal_keluar = tanggal_keluar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_status() {
        return id_status;
    }

    public void setId_status(int id_status) {
        this.id_status = id_status;
    }

    public int getId_pengirim() {
        return id_pengirim;
    }

    public void setId_pengirim(int id_pengirim) {
        this.id_pengirim = id_pengirim;
    }

    public String getResi() {
        return resi;
    }

    public void setResi(String resi) {
        this.resi = resi;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    public String getStatus_pengiriman() {
        return status_pengiriman;
    }

    public void setStatus_pengiriman(String status_pengiriman) {
        this.status_pengiriman = status_pengiriman;
    }


    @Override
    public String toString() {
        return "ListKurir{" +
                "id=" + id +
                ", id_status=" + id_status +
                ", id_pengirim=" + id_pengirim +
                ", resi='" + resi + '\'' +
                ", tujuan='" + tujuan + '\'' +
                ", status_pengiriman='" + status_pengiriman + '\'' +
                '}';
    }
}
