package com.wmi.substock1.model;

import java.util.List;

public class FormKurir {

    private String nama_penerima;
    private String no_telepon_penerima;
    private String bagian;
    private String note;

    public List<Gambar> getGambar() {
        return gambar;
    }

    public void setGambar(List<Gambar> gambar) {
        this.gambar = gambar;
    }

    private List<Gambar> gambar;

    public String getNama_penerima() {
        return nama_penerima;
    }

    public void setNama_penerima(String nama_penerima) {
        this.nama_penerima = nama_penerima;
    }

    public String getNo_telepon_penerima() {
        return no_telepon_penerima;
    }

    public void setNo_telepon_penerima(String no_telepon_penerima) {
        this.no_telepon_penerima = no_telepon_penerima;
    }

    public String getBagian() {
        return bagian;
    }

    public void setBagian(String bagian) {
        this.bagian = bagian;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
