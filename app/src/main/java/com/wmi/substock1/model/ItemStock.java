package com.wmi.substock1.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ItemStock {
    public String getWaktu_simpan() {
        return waktu_simpan;
    }

    public void setWaktu_simpan(String waktu_simpan) {
        this.waktu_simpan = waktu_simpan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama_produk() {
        return nama_produk;
    }

    public void setNama_produk(String nama_produk) {
        this.nama_produk = nama_produk;
    }

    public String getKode_produk() {
        return kode_produk;
    }

    public void setKode_produk(String kode_produk) {
        this.kode_produk = kode_produk;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getSatuan_isi() {
        return satuan_isi;
    }

    public void setSatuan_isi(String satuan_isi) {
        this.satuan_isi = satuan_isi;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    private String waktu_simpan;
    private int id;
    @SerializedName("t_hasil_barang_masuk_nama_produk")
    private String nama_produk;
    @SerializedName("t_hasil_barang_masuk_kode_produk")
    private String kode_produk;
    @SerializedName("t_hasil_barang_masuk_brand")
    private String brand;
    @SerializedName("t_hasil_barang_masuk_kategori")
    private String kategori;
    @SerializedName("t_hasil_barang_masuk_satuan_isi")
    private String satuan_isi;
    @SerializedName("saldo_total")
    private String total;

    public List<ItemUnik> getList() {
        return list;
    }

    @Override
    public String toString() {
        return "ItemStock{" +
                "waktu_simpan='" + waktu_simpan + '\'' +
                ", id=" + id +
                ", nama_produk='" + nama_produk + '\'' +
                ", kode_produk='" + kode_produk + '\'' +
                ", brand='" + brand + '\'' +
                ", kategori='" + kategori + '\'' +
                ", satuan_isi='" + satuan_isi + '\'' +
                ", total='" + total + '\'' +
                ", list=" + list +
                '}';
    }

    public void setList(List<ItemUnik> list) {
        this.list = list;
    }

    private List<ItemUnik> list;

}
