package com.wmi.substock1.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DetailKurir {

    private String asal_gudang;
    private String kendaraan;

    public String getKendaraan() {
        return kendaraan;
    }

    public void setKendaraan(String kendaraan) {
        this.kendaraan = kendaraan;
    }

    private int id_status;

    public int getId_status() {
        return id_status;
    }

    public void setId_status(int id_status) {
        this.id_status = id_status;
    }

    private String alamat_customer;
    private String alamat_gudang;
    private String resi;
    @SerializedName("telepon_customer")
    private String telpon_customer;
    private String keterangan;
    private String nama_gudang;
    private String nama_customer;
    private int id;
    private String tujuan;

    @SerializedName("list")
    private List<DetailBarangKurir> detailBarangKurirs;

    public List<DetailBarangKurir> getDetailBarangKurirs() {
        return detailBarangKurirs;
    }

    public void setDetailBarangKurirs(List<DetailBarangKurir> detailBarangKurirs) {
        this.detailBarangKurirs = detailBarangKurirs;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAsal_gudang() {
        return asal_gudang;
    }

    public void setAsal_gudang(String asal_gudang) {
        this.asal_gudang = asal_gudang;
    }

    public String getAlamat_customer() {
        return alamat_customer;
    }

    public void setAlamat_customer(String alamat_customer) {
        this.alamat_customer = alamat_customer;
    }

    public String getAlamat_gudang() {
        return alamat_gudang;
    }

    public void setAlamat_gudang(String alamat_gudang) {
        this.alamat_gudang = alamat_gudang;
    }

    public String getResi() {
        return resi;
    }

    public void setResi(String resi) {
        this.resi = resi;
    }

    public String getTelpon_customer() {
        return telpon_customer;
    }

    public void setTelpon_customer(String telpon_customer) {
        this.telpon_customer = telpon_customer;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getNama_gudang() {
        return nama_gudang;
    }

    public void setNama_gudang(String nama_gudang) {
        this.nama_gudang = nama_gudang;
    }

    public String getNama_customer() {
        return nama_customer;
    }

    public void setNama_customer(String nama_customer) {
        this.nama_customer = nama_customer;
    }
}
