package com.wmi.substock1.model;

import com.google.gson.annotations.SerializedName;

public class DetailBarangKurir {

    private String kode_produk;
    private String nama_produk;
    private String brand;
    private String jumlah_isi;
    private String satuan_isi;

    public String getKode_produk() {
        return kode_produk;
    }

    public void setKode_produk(String kode_produk) {
        this.kode_produk = kode_produk;
    }

    public String getNama_produk() {
        return nama_produk;
    }

    public void setNama_produk(String nama_produk) {
        this.nama_produk = nama_produk;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getJumlah_isi() {
        return jumlah_isi;
    }

    public void setJumlah_isi(String jumlah_isi) {
        this.jumlah_isi = jumlah_isi;
    }

    public String getSatuan_isi() {
        return satuan_isi;
    }

    public void setSatuan_isi(String satuan_isi) {
        this.satuan_isi = satuan_isi;
    }
}
