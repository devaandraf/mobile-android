package com.wmi.substock1.model;

import com.google.gson.annotations.SerializedName;

public class ItemUnik {

    private String id;
    @SerializedName("t_hasil_barang_masuk_kode_unik")
    private String kode_unik;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKode_unik() {
        return kode_unik;
    }

    public void setKode_unik(String kode_unik) {
        this.kode_unik = kode_unik;
    }

    public String getJumlah_isi() {
        return jumlah_isi;
    }

    public void setJumlah_isi(String jumlah_isi) {
        this.jumlah_isi = jumlah_isi;
    }

    public String getSatuan_isi() {
        return satuan_isi;
    }

    public void setSatuan_isi(String satuan_isi) {
        this.satuan_isi = satuan_isi;
    }

    public String getKadaluarsa() {
        return kadaluarsa;
    }

    public void setKadaluarsa(String kadaluarsa) {
        this.kadaluarsa = kadaluarsa;
    }

    public String getNomor_lot() {
        return nomor_lot;
    }

    @Override
    public String toString() {
        return "ItemUnik{" +
                "id='" + id + '\'' +
                ", kode_unik='" + kode_unik + '\'' +
                ", jumlah_isi='" + jumlah_isi + '\'' +
                ", satuan_isi='" + satuan_isi + '\'' +
                ", kadaluarsa='" + kadaluarsa + '\'' +
                ", nomor_lot='" + nomor_lot + '\'' +
                '}';
    }

    public void setNomor_lot(String nomor_lot) {
        this.nomor_lot = nomor_lot;
    }

    @SerializedName("t_hasil_barang_masuk_jumlah_isi")
    private String jumlah_isi;
    @SerializedName("t_hasil_barang_masuk_satuan_isi")
    private String satuan_isi;
    @SerializedName("t_hasil_barang_masuk_tanggal_kadaluarsa")
    private String kadaluarsa;
    @SerializedName("t_hasil_barang_masuk_no_lot_sn")
    private String nomor_lot;
}

