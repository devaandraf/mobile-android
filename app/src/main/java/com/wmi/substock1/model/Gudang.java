package com.wmi.substock1.model;

import com.google.gson.annotations.SerializedName;

public class Gudang {

    private String kode;

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNamaGudang() {
        return namaGudang;
    }

    public void setNamaGudang(String namaGudang) {
        this.namaGudang = namaGudang;
    }

    @SerializedName("nama")
    private String namaGudang;

    @Override
    public String toString() {
        return "Gudang{" +
                "namaGudang='" + namaGudang + '\'' +
                '}';
    }
}
