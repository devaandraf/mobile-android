package com.wmi.substock1.model;

public class LoginUsage {

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(String expires_in) {
        this.expires_in = expires_in;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private String token;
    private String expires_in;
    private String type;

    @Override
    public String toString() {
        return "LoginUsage{" +
                "token='" + token + '\'' +
                '}';
    }
}