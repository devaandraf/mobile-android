package com.wmi.substock1;

import com.wmi.substock1.model.DetailKurir;
import com.wmi.substock1.model.FormKurir;
import com.wmi.substock1.model.Gudang;
import com.wmi.substock1.model.ItemStock;
import com.wmi.substock1.model.ListKurir;
import com.wmi.substock1.model.LoginUsage;
import com.wmi.substock1.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MyService {

    @GET("stock")
    Call<MyMultipleResponse<ItemStock>> getListItemStock(@Header("Authorization") String token,
                                                         @Query("data") String data,
                                                         @Query("tgl_awal") String tglAwal,
                                                         @Query("tgl_akhir") String tglAkhir,
                                                         @Query("kode") String kode);

    @POST("stock/{kode}")
    Call<MySingleResponse<ItemStock>> getDetailItemStock(@Header("Authorization") String token,
                                                         @Path("kode") String kodeBarang);

    @FormUrlEncoded
    @POST("login")
    Call<MySingleResponse<LoginUsage>> login(@Field("username") String username,
                                             @Field("password") String password);

    @GET("user")
    Call<MySingleResponse<User>> user(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST("edit/{id}")
    Call<MySingleResponse<User>> edit(@Header("Authorization") String Token,
                                      @Path("id") int id,
                                      @Field("username") String username,
                                      @Field("password") String password);

    @GET("gudang")
    Call<MyMultipleResponse<Gudang>> gudang(@Header("Authorization") String token);

    @GET("kurir")
    Call<MyMultipleResponse<ListKurir>> getListKurir(@Header("Authorization") String token);

    @GET("kurir/{id}")
    Call<MySingleResponse<DetailKurir>> getDetailKurir(@Header("Authorization") String token,
                                                       @Path("id") int id);

    @FormUrlEncoded
    @POST("kendaraan/{id}")
    Call<MySingleResponse<DetailKurir>> sendBarangKurir(@Header("Authorization") String token,
                                                        @Path("id") int id,
                                                        @Field("kendaraan") String kendaraan);

    @POST("kurir/update/{id}")
    Call<MySingleResponse<FormKurir>> sendFormKurir(@Header("Authorization") String token,
                                                    @Path("id") int id,
                                                    @Body FormKurir data);
}
