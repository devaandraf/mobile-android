package com.wmi.substock1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wmi.substock1.model.LoginUsage;
import com.wmi.substock1.model.User;
import com.wmi.substock1.ui.home.HomeFragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    Button btnLogin;
    TextView et_username, et_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        et_username = (EditText) findViewById(R.id.et_username);
        et_password = (EditText) findViewById(R.id.et_password);

        String hakAkses = SessionHelper.getInstance(this).getUser().getHakAkses();
        if (hakAkses != null) {
            openHome(hakAkses);
        }

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validasiInput();
            }
        });

    }

    private void requestLogin(String username, String password){
        MyService service = MyRetrofit.getClient().create(MyService.class);
        service.login(username, password).enqueue(new Callback<MySingleResponse<LoginUsage>>() {
            @Override
            public void onResponse(Call<MySingleResponse<LoginUsage>> call, Response<MySingleResponse<LoginUsage>> response) {
                if (response.body().isSuccess()) {
                    String token = response.body().getValue().getToken();
                    SessionHelper.getInstance(Login.this).saveToken(token);
                    request(token);
                }
                else {
                    showMessage("Username atau Password Salah");
                }
            }

            @Override
            public void onFailure(Call<MySingleResponse<LoginUsage>> call, Throwable t) {

            }
        });
    }

    private void request(String token){
        MyService service = MyRetrofit.getClient().create(MyService.class);
        service.user("Bearer " + token).enqueue(new Callback<MySingleResponse<User>>() {
            @Override
            public void onResponse(Call<MySingleResponse<User>> call, Response<MySingleResponse<User>> response) {
                if (response.body().isSuccess()){
                    User user = response.body().getValue();
                    SessionHelper.getInstance(Login.this).saveUser(user);
                    openHome(user.getHakAkses());
                }
            }

            @Override
            public void onFailure(Call<MySingleResponse<User>> call, Throwable t) {

            }
        });
    }

    public void openHome(String hakAkses) {
        if (hakAkses.equals("marketing")) {
            Intent intent = new Intent(Login.this, DrawerActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void validasiInput() {
        String username = et_username.getText().toString();
        String password = et_password.getText().toString();
        if (username.isEmpty() || password.isEmpty()){
            showMessage("Username atau Password harus diisi !");
        }
        else {
            requestLogin(username, password);
        }
    }
}