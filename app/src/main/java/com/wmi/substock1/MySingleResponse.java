package com.wmi.substock1;

public class MySingleResponse<T> {

    private boolean success;
    private String message;
    private T value;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "MySingleResponse{" +
                "success=" + success +
                ", message='" + message + '\'' +
                ", value=" + value +
                '}';
    }
}
