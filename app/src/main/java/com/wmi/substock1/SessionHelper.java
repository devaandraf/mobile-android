package com.wmi.substock1;

import android.content.Context;
import android.content.SharedPreferences;

import com.wmi.substock1.model.User;

public class SessionHelper {
    private static SessionHelper instance;
    private SharedPreferences sharedPreferences;

    public static SessionHelper getInstance(Context context) {
        if (instance == null) {
            instance = new SessionHelper(context);
        }
        return instance;
    }

    public SessionHelper(Context context) {
        sharedPreferences = context.getSharedPreferences("my_preferences", Context.MODE_PRIVATE);
    }

    public void saveToken(String token) {
        sharedPreferences.edit().putString("token", token).apply();
    }

    public void saveUser(User user) {
        sharedPreferences.edit().putString("nama", user.getNama()).apply();
        sharedPreferences.edit().putString("hak_akses", user.getHakAkses()).apply();
        sharedPreferences.edit().putString("username", user.getUsername()).apply();
        sharedPreferences.edit().putString("penempatan", user.getPenempatan()).apply();
        sharedPreferences.edit().putInt("id_user", user.getId_user()).apply();
        sharedPreferences.edit().putInt("id_pegawai", user.getId_pegawai()).apply();
    }

    public User getUser() {
        User user = new User();
        user.setNama(sharedPreferences.getString("nama", null));
        user.setHakAkses(sharedPreferences.getString("hak_akses", null));
        user.setUsername(sharedPreferences.getString("username", null));
        user.setUsername(sharedPreferences.getString("penempatan", null));
        user.setId_user(sharedPreferences.getInt("id_user", 0));
        user.setId_pegawai(sharedPreferences.getInt("id_pegawai", 0));
        return user;
    }

    public String getToken() {
        return sharedPreferences.getString("token", null);
    }

    public void removeSession() {
        sharedPreferences.edit().remove("token").apply();
        sharedPreferences.edit().remove("nama").apply();
        sharedPreferences.edit().remove("hak_akses").apply();
        sharedPreferences.edit().remove("username").apply();
        sharedPreferences.edit().remove("penempatan").apply();
        sharedPreferences.edit().remove("id_user").apply();
        sharedPreferences.edit().remove("id_pegawai").apply();
    }
}
