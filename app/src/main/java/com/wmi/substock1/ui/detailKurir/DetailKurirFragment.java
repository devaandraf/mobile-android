package com.wmi.substock1.ui.detailKurir;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wmi.substock1.MyRetrofit;
import com.wmi.substock1.MyService;
import com.wmi.substock1.MySingleResponse;
import com.wmi.substock1.R;
import com.wmi.substock1.SessionHelper;
import com.wmi.substock1.adapter.MyAdapter_Detail_Kurir;
import com.wmi.substock1.adapter.MyAdapter_Kurir;
import com.wmi.substock1.model.DetailBarangKurir;
import com.wmi.substock1.model.DetailKurir;
import com.wmi.substock1.model.ItemStock;
import com.wmi.substock1.model.ItemUnik;
import com.wmi.substock1.model.ListKurir;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailKurirFragment extends Fragment {

    RecyclerView recyclerView;
    MyAdapter_Detail_Kurir myAdapter_detail_kurir;
    String token;
    TextView namacustomer, alamat, notelp, resi, asalgudang, keterangan;
    EditText kendaraan;
    Button btnKirim;
    int id_barang_keluar;
    int id;
    public static final String EXTRA_ID = "extra_id";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_detail_kurir, container, false);

        recyclerView = root.findViewById(R.id.recyclerview);
        namacustomer = root.findViewById(R.id.namacust2);
        keterangan = root.findViewById(R.id.keterangan2);
        alamat = root.findViewById(R.id.alamat2);
        notelp = root.findViewById(R.id.telpon2);
        resi = root.findViewById(R.id.resigudang2);
        asalgudang = root.findViewById(R.id.asalgudang2);
        kendaraan = root.findViewById(R.id.et_kendaraan);
        btnKirim = root.findViewById(R.id.btn_kirim);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (kendaraan.getText().toString().isEmpty())
                {
                    showMessage("Kendaraan Harus Diisi !");
                } else
                    {
                        sendBarangKurir(kendaraan.getText().toString());
                    }
            }
        });

        token = "Bearer" + SessionHelper.getInstance(getContext()).getToken();
        id = getArguments().getInt(EXTRA_ID);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        myAdapter_detail_kurir = new MyAdapter_Detail_Kurir();
        recyclerView.setAdapter(myAdapter_detail_kurir);

        requestDetail();

    }

    private void requestDetail()
    {
        MyService service = MyRetrofit.getClient().create(MyService.class);
        service.getDetailKurir(token, id).enqueue(new Callback<MySingleResponse<DetailKurir>>() {
            @Override
            public void onResponse(Call<MySingleResponse<DetailKurir>> call, Response<MySingleResponse<DetailKurir>> response) {
                if(response.body().isSuccess())
                {
                    DetailKurir detailKurir = response.body().getValue();
                    setDataBarang(detailKurir);
                }
            }

            @Override
            public void onFailure(Call<MySingleResponse<DetailKurir>> call, Throwable t) {

            }
        });
    }

    private void setDataBarang(DetailKurir detailKurir)
    {
        id_barang_keluar = detailKurir.getId();
        myAdapter_detail_kurir.setListKurirs((ArrayList<DetailBarangKurir>) detailKurir.getDetailBarangKurirs());
        if (detailKurir.getTujuan().equals("Gudang"))
        {
            alamat.setText(detailKurir.getAlamat_gudang());
            namacustomer.setText(detailKurir.getNama_gudang());
            notelp.setText("-");
        }
        else
            {
                alamat.setText(detailKurir.getAlamat_customer());
                namacustomer.setText(detailKurir.getNama_customer());
                notelp.setText(detailKurir.getTelpon_customer());
            }
        keterangan.setText(detailKurir.getKeterangan());
        resi.setText(detailKurir.getResi());
        asalgudang.setText(detailKurir.getAsal_gudang());
    }

    private void showMessage(String message)
    {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void sendBarangKurir(String kendaraan)
    {
        MyService service = MyRetrofit.getClient().create(MyService.class);
        service.sendBarangKurir(token, id_barang_keluar, kendaraan).enqueue(new Callback<MySingleResponse<DetailKurir>>() {
            @Override
            public void onResponse(Call<MySingleResponse<DetailKurir>> call, Response<MySingleResponse<DetailKurir>> response) {
                if (response.body().isSuccess())
                {
                    showMessage(response.body().getMessage());
                    getActivity().onBackPressed();
                }
            }

            @Override
            public void onFailure(Call<MySingleResponse<DetailKurir>> call, Throwable t) {

            }
        });
    }
}