package com.wmi.substock1.ui.filter;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.wmi.substock1.MyMultipleResponse;
import com.wmi.substock1.MyRetrofit;
import com.wmi.substock1.MyService;
import com.wmi.substock1.R;
import com.wmi.substock1.SessionHelper;
import com.wmi.substock1.model.Gudang;
import com.wmi.substock1.ui.stock.StockFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class FilterFragment extends Fragment {

    EditText etPeriode;
    Button btnSimpan, btnReset;
    String startDate;
    String endDate;
    String kode;
    String token;
    ChipGroup cgGudang;
    int year;
    int month;
    int day;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_filter, container, false);

        etPeriode = root.findViewById(R.id.et_periode);
        btnSimpan = root.findViewById(R.id.simpan);
        btnReset = root.findViewById(R.id.reset);
        cgGudang = root.findViewById(R.id.cg_gudang);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Calendar calendar = Calendar.getInstance();

        MaterialDatePicker.Builder<Pair<Long, Long>> builder = MaterialDatePicker.Builder.dateRangePicker();
        builder.setTitleText("Pilih Tanggal");
        final MaterialDatePicker materialDatePicker = builder.build();
        materialDatePicker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener() {
            @Override
            public void onPositiveButtonClick(Object selection) {
                Pair<Long, Long> pickerRange = (Pair<Long, Long>) selection;
                startDate = formatLongToString(pickerRange.first);
                endDate = formatLongToString(pickerRange.second);

                etPeriode.setText(startDate + "   Sampai   " + endDate);
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed(); }
        });

        token = "Bearer " + SessionHelper.getInstance(getContext()).getToken();

        requestGudang();

        etPeriode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                materialDatePicker.show(getChildFragmentManager(), "DATE_PICKER");
            }
        });

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle result = new Bundle();
                result.putString("tglAwal", startDate);
                result.putString("tglAkhir", endDate);
                result.putString("kode", kode);
                getParentFragmentManager().setFragmentResult("filter", result);
                StockFragment stockFragment = new StockFragment();
                getActivity().onBackPressed();
            }
        });

    }

    private String formatLongToString(Long date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date(date));
    }

    private void requestGudang()
    {
        MyService service = MyRetrofit.getClient().create(MyService.class);
        service.gudang(token).enqueue(new Callback<MyMultipleResponse<Gudang>>() {
            @Override
            public void onResponse(Call<MyMultipleResponse<Gudang>> call, Response<MyMultipleResponse<Gudang>> response) {
                if (response.body().isSuccess())
                {
                    List<Gudang> gudangs = response.body().getValue();
                    setGudangChips(gudangs);
                }
            }

            @Override
            public void onFailure(Call<MyMultipleResponse<Gudang>> call, Throwable t) {
                showMessage(t.getMessage());
            }
        });
    }

    private void setGudangChips(List<Gudang> gudangs) {
        for (Gudang gudang: gudangs) {
            Chip chip = new Chip(requireContext());
            chip.setText(gudang.getNamaGudang());
            chip.setCheckable(true);
            chip.setClickable(true);
            chip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (isChecked) {
                        kode = gudang.getKode();
                        showMessage(kode);
                    }
                }
            });
            if (chip.isChecked()) {
                kode = gudang.getKode();
                showMessage(kode);
            }
            cgGudang.addView(chip);
        }
    }

    private void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

}