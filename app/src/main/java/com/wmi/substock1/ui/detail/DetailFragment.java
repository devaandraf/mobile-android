package com.wmi.substock1.ui.detail;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wmi.substock1.adapter.MyAdapter_Detail_Stock;
import com.wmi.substock1.MyRetrofit;
import com.wmi.substock1.MyService;
import com.wmi.substock1.MySingleResponse;
import com.wmi.substock1.R;
import com.wmi.substock1.SessionHelper;
import com.wmi.substock1.model.ItemStock;
import com.wmi.substock1.model.ItemUnik;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailFragment extends Fragment {

    RecyclerView recyclerView2;
    private RecyclerView.LayoutManager layoutManager;
    public static final String EXTRA_KODE = "extra_kode";
    MyAdapter_Detail_Stock myAdapter_detailStock;
    ArrayList<ItemUnik> uniks;
    TextView nama_barang, kategori, brand, kode_produk, tvNama;
    String token;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_detail, container, false);

        tvNama = root.findViewById(R.id.tv_nama);
        nama_barang = root.findViewById(R.id.barang);
        kategori = root.findViewById(R.id.kategori);
        brand = root.findViewById(R.id.brand);
        kode_produk = root.findViewById(R.id.kode);
        recyclerView2 = (RecyclerView) root.findViewById(R.id.recyclerview2);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String nama = SessionHelper.getInstance(getContext()).getUser().getNama();
        tvNama.setText("Hai, " + nama);

        token = "Bearer " + SessionHelper.getInstance(getContext()).getToken();

        recyclerView2.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView2.setLayoutManager(layoutManager);
        myAdapter_detailStock = new MyAdapter_Detail_Stock();
        recyclerView2.setAdapter(myAdapter_detailStock);

        String kodeProduk = getArguments().getString(EXTRA_KODE);
        request(kodeProduk);

    }

    private void request(String kodeProduk) {
        MyService service = MyRetrofit.getClient().create(MyService.class);
        service.getDetailItemStock(token, kodeProduk).enqueue(new Callback<MySingleResponse<ItemStock>>() {
            @Override
            public void onResponse(Call<MySingleResponse<ItemStock>> call, Response<MySingleResponse<ItemStock>> response) {
                ItemStock stock = response.body().getValue();
                Log.d("Detail", stock.toString());
                setData(stock);
            }

            @Override
            public void onFailure(Call<MySingleResponse<ItemStock>> call, Throwable t) {

            }
        });
    }

    private void setData(ItemStock stock) {
//        Toast.makeText(this, stock.toString(), Toast.LENGTH_LONG).show();

        nama_barang.setText(stock.getNama_produk());
        kategori.setText(stock.getKategori());
        brand.setText(stock.getBrand());
        kode_produk.setText(stock.getKode_produk());
        myAdapter_detailStock.setUniks((ArrayList<ItemUnik>) stock.getList());
    }
}