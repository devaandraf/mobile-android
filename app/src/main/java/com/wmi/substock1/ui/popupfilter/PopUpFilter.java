package com.wmi.substock1.ui.popupfilter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.wmi.substock1.MyMultipleResponse;
import com.wmi.substock1.MyRetrofit;
import com.wmi.substock1.MyService;
import com.wmi.substock1.R;
import com.wmi.substock1.SessionHelper;
import com.wmi.substock1.model.Gudang;
import com.wmi.substock1.ui.stock.StockFragment;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PopUpFilter extends DialogFragment {

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    private OnClickListener onClickListener;
    EditText etPeriode;
    Button btnSimpan, btnReset;
    String startDate;
    String endDate;
    String kode;
    String token;
    ChipGroup cgGudang;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = requireActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.popupfilter, null);

        etPeriode = view.findViewById(R.id.et_periode);
        btnSimpan = view.findViewById(R.id.simpan);
        btnReset = view.findViewById(R.id.reset);
        cgGudang = view.findViewById(R.id.cg_gudang);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(view);
                // Add action buttons

        MaterialDatePicker.Builder<Pair<Long, Long>> dateRangePicker = MaterialDatePicker.Builder.dateRangePicker();
        dateRangePicker.setTitleText("Pilih Tanggal");
        final MaterialDatePicker materialDatePicker = dateRangePicker.build();
        materialDatePicker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener() {
            @Override
            public void onPositiveButtonClick(Object selection) {
                Pair<Long, Long> pickerRange = (Pair<Long, Long>) selection;
                startDate = formatLongToString(pickerRange.first);
                endDate = formatLongToString(pickerRange.second);

                etPeriode.setText(startDate + "   Sampai   " + endDate);
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onResetClicked();
                dismiss();
            }
        });

        token = "Bearer " + SessionHelper.getInstance(getContext()).getToken();

        requestGudang();

        etPeriode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                materialDatePicker.show(getChildFragmentManager(), "DATE_PICKER");
            }
        });


        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onSaveClicked(startDate, endDate, kode);
                dismiss();
            }
        });

        return builder.create();
    }

    private void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void setGudangChips(List<Gudang> gudangs) {
        for (Gudang gudang: gudangs) {
            Chip chip = new Chip(requireContext());
            chip.setText(gudang.getNamaGudang());
            chip.setCheckable(true);
            chip.setClickable(true);
            chip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (isChecked) {
                        kode = gudang.getKode();
                        showMessage(kode);
                    }
                }
            });
            if (chip.isChecked()) {
                kode = gudang.getKode();
                showMessage(kode);
            }
            cgGudang.addView(chip);
        }
    }

    private void requestGudang()
    {
        MyService service = MyRetrofit.getClient().create(MyService.class);
        service.gudang(token).enqueue(new Callback<MyMultipleResponse<Gudang>>() {
            @Override
            public void onResponse(Call<MyMultipleResponse<Gudang>> call, Response<MyMultipleResponse<Gudang>> response) {
                if (response.body().isSuccess())
                {
                    List<Gudang> gudangs = response.body().getValue();
                    setGudangChips(gudangs);
                }
            }

            @Override
            public void onFailure(Call<MyMultipleResponse<Gudang>> call, Throwable t) {
                showMessage(t.getMessage());
            }
        });
    }

    private String formatLongToString(Long date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date(date));
    }

    public interface OnClickListener {
        void onSaveClicked(String tglAwal, String tglAkhir, String kode);
        void onResetClicked();
    }
}
