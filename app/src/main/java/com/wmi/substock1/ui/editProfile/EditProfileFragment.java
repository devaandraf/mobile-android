package com.wmi.substock1.ui.editProfile;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.wmi.substock1.MyRetrofit;
import com.wmi.substock1.MyService;
import com.wmi.substock1.MySingleResponse;
import com.wmi.substock1.R;
import com.wmi.substock1.SessionHelper;
import com.wmi.substock1.model.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class EditProfileFragment extends Fragment {

    EditText etUsername, etPassword;
    String token;
    int id;
    Button btnSimpan;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_edit_profile, container, false);

        etUsername = root.findViewById(R.id.et_username);
        etPassword = root.findViewById(R.id.et_password);
        btnSimpan = root.findViewById(R.id.btn_simpan);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        token = "Bearer " + SessionHelper.getInstance(getContext()).getToken();
        id = SessionHelper.getInstance(getContext()).getUser().getId_user();

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validasiInput();
            }
        });
    }

    public void validasiInput() {
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();
        if (username.isEmpty() || password.isEmpty()){
            showMessage("Username atau Password harus diisi !");
        }
        else {
            requestEdit(username, password);
        }
    }

    private void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    private void requestEdit(String username, String password)
    {
        MyService service = MyRetrofit.getClient().create(MyService.class);
        service.edit(token, id, username, password).enqueue(new Callback<MySingleResponse<User>>() {
            @Override
            public void onResponse(Call<MySingleResponse<User>> call, Response<MySingleResponse<User>> response) {
                if (response.body().isSuccess())
                {
                    showMessage("Username atau Password berhasil diganti");
                }

            }

            @Override
            public void onFailure(Call<MySingleResponse<User>> call, Throwable t) {

            }
        });
    }

}