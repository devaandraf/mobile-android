package com.wmi.substock1.ui.kurir;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class KurirViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public KurirViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is kurir fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}