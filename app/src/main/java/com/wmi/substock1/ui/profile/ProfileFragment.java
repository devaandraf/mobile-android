package com.wmi.substock1.ui.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.wmi.substock1.R;
import com.wmi.substock1.SessionHelper;

public class ProfileFragment extends Fragment {

    Button btnEdit;
    TextView tvNama, tvUsername, tvPenempatan;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profile, container, false);

        btnEdit = root.findViewById(R.id.btn_edit);
        tvNama = root.findViewById(R.id.tv_nama);
        tvUsername = root.findViewById(R.id.tv_username);
        tvPenempatan = root.findViewById(R.id.tv_penempatan);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_nav_profile_to_editProfileFragment);
            }
        });

        String nama = SessionHelper.getInstance(getContext()).getUser().getNama();
        tvNama.setText(nama);

        String username = SessionHelper.getInstance(getContext()).getUser().getUsername();
        tvUsername.setText(username);

        String penempatan = SessionHelper.getInstance(getContext()).getUser().getPenempatan();
        tvPenempatan.setText(penempatan);

    }
}
