package com.wmi.substock1.ui.validasi;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ValidasiViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ValidasiViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is validasi fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}