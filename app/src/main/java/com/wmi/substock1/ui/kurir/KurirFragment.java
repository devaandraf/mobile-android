package com.wmi.substock1.ui.kurir;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.wmi.substock1.MyMultipleResponse;
import com.wmi.substock1.MyRetrofit;
import com.wmi.substock1.MyService;
import com.wmi.substock1.R;
import com.wmi.substock1.SessionHelper;
import com.wmi.substock1.adapter.MyAdapter_Kurir;
import com.wmi.substock1.adapter.MyAdapter_Stock;
import com.wmi.substock1.model.ListKurir;
import com.wmi.substock1.ui.detailKurir.DetailKurirFragment;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KurirFragment extends Fragment implements MyAdapter_Kurir.OnClickListener {

    RecyclerView recyclerView;
    MyAdapter_Kurir myAdapter_kurir;
    String token;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_kurir, container, false);

        recyclerView = root.findViewById(R.id.recyclerview);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        token = "Bearer" + SessionHelper.getInstance(getContext()).getToken();

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        myAdapter_kurir = new MyAdapter_Kurir();
        myAdapter_kurir.setOnclick(this);
        recyclerView.setAdapter(myAdapter_kurir);

        requestKurir();
    }

    private void requestKurir()
    {
        MyService service = MyRetrofit.getClient().create(MyService.class);
        int id = SessionHelper.getInstance(getContext()).getUser().getId_pegawai();
        service.getListKurir(token).enqueue(new Callback<MyMultipleResponse<ListKurir>>() {
            @Override
            public void onResponse(Call<MyMultipleResponse<ListKurir>> call, Response<MyMultipleResponse<ListKurir>> response) {
                if (response.body().isSuccess())
                {
                    ArrayList<ListKurir> kurirs = (ArrayList<ListKurir>) response.body().getValue();
                    myAdapter_kurir.setKurirs(kurirs);
                    showMessage(response.body().getMessage());
                }
                else
                    {
                        showMessage("List Kosong");
                        myAdapter_kurir.clearKurirs();
                    }
            }

            @Override
            public void onFailure(Call<MyMultipleResponse<ListKurir>> call, Throwable t) {

            }
        });
    }

    private void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(ListKurir listKurir) {
        Bundle bundle = new Bundle();
        bundle.putInt(DetailKurirFragment.EXTRA_ID, listKurir.getId());
        if (listKurir.getId_status() == 4)
        {
            Navigation.findNavController(getView()).navigate(R.id.action_nav_kurir_to_formKurirFragment, bundle);
        }
        else
            {
                Navigation.findNavController(getView()).navigate(R.id.action_nav_kurir_to_detailKurirFragment, bundle);
            }
    }
}