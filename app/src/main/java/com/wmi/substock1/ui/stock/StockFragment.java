package com.wmi.substock1.ui.stock;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentResultListener;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.wmi.substock1.Login;
import com.wmi.substock1.adapter.MyAdapter_Stock;
import com.wmi.substock1.MyMultipleResponse;
import com.wmi.substock1.MyRetrofit;
import com.wmi.substock1.MyService;
import com.wmi.substock1.R;
import com.wmi.substock1.SessionHelper;
import com.wmi.substock1.model.ItemStock;
import com.wmi.substock1.ui.detail.DetailFragment;
import com.wmi.substock1.ui.popupfilter.PopUpFilter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StockFragment extends Fragment implements MyAdapter_Stock.OnClickListener, PopUpFilter.OnClickListener {

    MyAdapter_Stock myAdapterStock;
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    ImageView filter2;
    EditText editText;
    String token;
    TextView tvNama;
    String tglAwal, tglAkhir;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_stock, container, false);

        tvNama = root.findViewById(R.id.tv_nama);
        editText = root.findViewById(R.id.edittext);
        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerview);
        filter2 = root.findViewById(R.id.filter2);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PopUpFilter filter = new  PopUpFilter();
        filter.setOnClickListener(this);
        filter2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filter.show(getChildFragmentManager(), "Filter");
            }
        });

        token = "Bearer " + SessionHelper.getInstance(getContext()).getToken();

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String data = s.toString();
                request(data, null, null, null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        myAdapterStock = new MyAdapter_Stock();
        myAdapterStock.setOnclick(this);
        recyclerView.setAdapter(myAdapterStock);

        request(null, null, null, null);
//        if (tglAkhir.isEmpty()){
////            request(null, null, null, null);
////        }

    }

    private String formatLongToString(Long date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date(date));
    }

    private void request(String data, String tglAwal, String tglAkhir, String kode) {
//        showMessage("tgl awal"+ tglAwal);
//        showMessage("tgl akhir "+ tglAkhir );
//        showMessage("data "+ data );
//        showMessage("kode "+ kode);
//        if(kode == null){
//
//        }
//        else {

        MyService service = MyRetrofit.getClient().create(MyService.class);
        service.getListItemStock(token, data, tglAwal, tglAkhir, kode).enqueue(new Callback<MyMultipleResponse<ItemStock>>() {
            @Override
            public void onResponse(Call<MyMultipleResponse<ItemStock>> call, Response<MyMultipleResponse<ItemStock>> response) {
                if (response.body().isSuccess()) {
                    ArrayList<ItemStock> stocks = (ArrayList<ItemStock>) response.body().getValue();
                    myAdapterStock.setStocks(stocks);
                    showMessage(response.body().getMessage());
                } else {
                    showMessage("Tidak Ada Barang");
                    myAdapterStock.clearStocks();
                }
            }

            @Override
            public void onFailure(Call<MyMultipleResponse<ItemStock>> call, Throwable t) {
                showMessage(t.getMessage());
            }
        });
    }

    @Override
    public void onclick(ItemStock itemStock) {
        Bundle bundle = new Bundle();
        bundle.putString(DetailFragment.EXTRA_KODE, itemStock.getKode_produk());
        Navigation.findNavController(getView()).navigate(R.id.action_nav_stock_to_detailFragment, bundle);
    }

    private void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSaveClicked(String tglAwal, String tglAkhir, String kode) {
        request(null, tglAwal, tglAkhir, kode);
    }

    @Override
    public void onResetClicked() {
        request(null, null, null, null);
    }
}